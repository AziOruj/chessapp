package azi.oruj.schools.controllers;

import azi.oruj.schools.entity.Students;
import azi.oruj.schools.entity.dto.StudentCreateDto;
import azi.oruj.schools.entity.dto.StudentDto;
import azi.oruj.schools.services.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.DateFormatter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping("/students")
public class StudentController {
    private final StudentService studentService;

    @PostMapping()
    public StudentDto create(@RequestBody StudentCreateDto create) {
        return studentService.create(create);
    }

    @PutMapping("/{id}")
    public StudentDto update(@PathVariable Long id, @RequestBody StudentCreateDto update) {
        return studentService.update(id, update);
    }

    @GetMapping("/{id}")
    public StudentDto get(@PathVariable Long id){
        return studentService.get(id);
    }

    @GetMapping()
    public List<StudentDto> getAll(){
        return studentService.getAll();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        studentService.delete(id);
    }

    @GetMapping("/{schoolId}/{whatTime}/{dailyDate}")
    public List<StudentDto> getStudentsSchools(@PathVariable("schoolId") Long schoolId,
                                         @PathVariable("whatTime") String whatTime,
                                         @PathVariable("dailyDate") String dailyDate){
//        return schoolId;
        LocalDate parse = LocalDate.parse(dailyDate);
        return studentService.getAllSchool(schoolId.toString(), whatTime,
                LocalDate.parse(dailyDate));
    }

}
