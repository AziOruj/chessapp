package azi.oruj.schools.controllers;

import azi.oruj.schools.entity.dto.SchoolCreateDto;
import azi.oruj.schools.entity.dto.SchoolDto;
import azi.oruj.schools.services.SchoolService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController
@RequestMapping("/school")
@RequiredArgsConstructor
public class SchoolController {
    private final SchoolService schoolService;

    @PostMapping
    public SchoolDto create(@RequestBody SchoolCreateDto create){
       return schoolService.create(create);
    }

    @PutMapping("/{id}")
    public SchoolDto update(@PathVariable Long id, @RequestBody SchoolCreateDto update){
        return schoolService.update(id,update);
    }

    @GetMapping("/{id}")
    public SchoolDto get(@PathVariable Long id){
        return schoolService.get(id);
    }

    @GetMapping()
    public List<String> getAll(){
        return schoolService.getAll();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        schoolService.delete(id);
    }


}
