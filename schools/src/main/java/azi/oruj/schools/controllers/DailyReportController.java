package azi.oruj.schools.controllers;

import azi.oruj.schools.entity.dto.AllDailyListDto;
import azi.oruj.schools.entity.dto.DailyReportCreateDto;
import azi.oruj.schools.entity.dto.DailyReportDto;
import azi.oruj.schools.services.DailyReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/dailyReports")
@CrossOrigin
public class DailyReportController {
    private final DailyReportService dailyReportService;

    @PostMapping()
    public List<DailyReportDto> createFew(@RequestBody DailyReportCreateDto createFew){
        return dailyReportService.createFew(createFew);
    }
    @GetMapping("/whatTimeList/{schoolId}")
    public List<String> createFew(@PathVariable String schoolId){
        return dailyReportService.getListWhatTime(schoolId);
    }
    @GetMapping("/getAllList")
    public List<AllDailyListDto> getAllList(){
        return dailyReportService.getAllDailyList();
    }

}
