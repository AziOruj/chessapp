package azi.oruj.schools.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data

@NoArgsConstructor
public class AllDailyListDto {
    private LocalDate dailyDate;
    private String schoolName;
    private String whatTime;

    public AllDailyListDto(LocalDate dailyDate, String schoolName, String whatTime) {
        this.dailyDate = dailyDate;
        this.schoolName = schoolName;
        this.whatTime = whatTime;
    }
}
