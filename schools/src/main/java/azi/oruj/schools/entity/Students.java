package azi.oruj.schools.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "students")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Students {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    private LocalDate beginDate;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private Boolean isActive;
    private String whatTime;
    @ManyToOne
    private SchoolEnt school;
    @JsonIgnore
    @OneToMany(mappedBy = "student")
    @ToString.Exclude
    private Set<DailyReport> dailyReports= new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Students students = (Students) o;
        return id != null && Objects.equals(id, students.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
