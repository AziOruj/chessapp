package azi.oruj.schools.entity.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SchoolDto {
    private Long id;
    private String schoolName;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private Boolean isActive;
    //private HashSet<StudentDto> students= new HashSet<>();
}
