package azi.oruj.schools.entity.dto;

import lombok.Data;

@Data
public class SchoolCreateDto {
    private String schoolName;
}
