package azi.oruj.schools.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "schools")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class SchoolEnt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String schoolName;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private Boolean isActive;
    @JsonIgnore
    @OneToMany(mappedBy = "school")
    @ToString.Exclude
    private Set<Students> students= new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        SchoolEnt schoolEnt = (SchoolEnt) o;
        return id != null && Objects.equals(id, schoolEnt.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
