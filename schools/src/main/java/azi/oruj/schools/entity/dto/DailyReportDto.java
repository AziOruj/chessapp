package azi.oruj.schools.entity.dto;

import azi.oruj.schools.entity.Students;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class DailyReportDto {
    private Long id;
    private LocalDate dailyDate;
    private Boolean isActive;
    private Boolean isAttended;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private StudentDto student;
}
