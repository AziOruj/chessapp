package azi.oruj.schools.entity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
@Data
@NotEmpty
public class StudentCreateDto {
    private String name;
    private String surname;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate beginDate;
    private String whatTime;
    private String schoolName;

}
