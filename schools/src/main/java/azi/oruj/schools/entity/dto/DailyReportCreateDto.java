package azi.oruj.schools.entity.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class DailyReportCreateDto {
    private List<DailyReportCreateBean> beanList;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate dailyDate;
    private String schoolName;
    private String whatTime;

}
