package azi.oruj.schools.entity.dto;

import azi.oruj.schools.entity.SchoolEnt;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class StudentDto {
    private Long id;
    private String name;
    private String surname;
    private LocalDate beginDate;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private Boolean isActive;
    private String whatTime;
    private SchoolDto school;
}
