package azi.oruj.schools.entity.dto;

import lombok.Data;

@Data
public class DailyReportCreateBean {
    private Boolean isAttended;
    private StudentDto student;
}
