package azi.oruj.schools.services;

import azi.oruj.schools.entity.dto.*;

import java.util.List;

public interface DailyReportService {
    List<DailyReportDto> createFew(DailyReportCreateDto create);
    List<DailyReportDto> updateFew(Long id, DailyReportCreateDto update);
    DailyReportDto getFew(Long id);
    void deleteFew(Long id);
    List<DailyReportDto> getAll();
    List<String> getListWhatTime(String school);
    List<AllDailyListDto> getAllDailyList();


}
