package azi.oruj.schools.services;

import azi.oruj.schools.entity.Students;
import azi.oruj.schools.entity.dto.StudentCreateDto;
import azi.oruj.schools.entity.dto.StudentDto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface StudentService {
     StudentDto create (StudentCreateDto create);
     StudentDto update (Long id, StudentCreateDto update);
     StudentDto get(Long id);
     List<StudentDto> getAll();
     void delete(Long id);
     List<StudentDto> getAllSchool(String school, String whatTime, LocalDate beginDate);

}
