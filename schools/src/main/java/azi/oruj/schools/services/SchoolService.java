package azi.oruj.schools.services;

import azi.oruj.schools.entity.dto.SchoolCreateDto;
import azi.oruj.schools.entity.dto.SchoolDto;

import java.util.List;

public interface SchoolService {
    SchoolDto create(SchoolCreateDto create);
    SchoolDto update(Long id, SchoolCreateDto update);
    SchoolDto get(Long id);
    void delete(Long id);
    List<String> getAll();
}
