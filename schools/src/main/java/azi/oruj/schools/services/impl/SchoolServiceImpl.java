package azi.oruj.schools.services.impl;

import azi.oruj.schools.entity.SchoolEnt;
import azi.oruj.schools.entity.dto.SchoolCreateDto;
import azi.oruj.schools.entity.dto.SchoolDto;
import azi.oruj.schools.exceptions.SchoolNotFoundException;
import azi.oruj.schools.repo.SchoolsRepo;
import azi.oruj.schools.services.SchoolService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SchoolServiceImpl implements SchoolService {
    private final SchoolsRepo schoolsRepo;
    private final ModelMapper mapper;
    @Override
    public SchoolDto create(SchoolCreateDto create) {
        SchoolEnt schoolEnt =mapper.map(create,SchoolEnt.class);
        schoolEnt.setCreateDate(LocalDateTime.now());
        schoolEnt.setIsActive(true);
        return mapper.map(schoolsRepo.save(schoolEnt),SchoolDto.class);
    }

    @Override
    public SchoolDto update(Long id, SchoolCreateDto update) {
        SchoolEnt schoolEnt = schoolsRepo.findById(id).orElseThrow(SchoolNotFoundException::new);
        schoolEnt.setSchoolName(update.getSchoolName());
        schoolEnt.setUpdateDate(LocalDateTime.now());
        return mapper.map(schoolsRepo.save(schoolEnt),SchoolDto.class);
    }

    @Override
    public SchoolDto get(Long id) {
        return mapper.map(schoolsRepo.findById(id).orElseThrow(SchoolNotFoundException::new), SchoolDto.class);
    }

    @Override
    public void delete(Long id) {
        if (schoolsRepo.existsById(id)){
            schoolsRepo.deleteById(id);
        }else {
            throw new SchoolNotFoundException();
        }
    }

    @Override
    public List<String> getAll() {
        return schoolsRepo.findAllString();
    }
}
