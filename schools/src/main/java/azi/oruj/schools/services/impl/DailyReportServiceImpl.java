package azi.oruj.schools.services.impl;

import azi.oruj.schools.entity.DailyReport;
import azi.oruj.schools.entity.SchoolEnt;
import azi.oruj.schools.entity.Students;
import azi.oruj.schools.entity.dto.*;
import azi.oruj.schools.exceptions.SchoolNotFoundException;
import azi.oruj.schools.repo.DailyReportRepo;
import azi.oruj.schools.repo.SchoolsRepo;
import azi.oruj.schools.repo.StudentsRepo;
import azi.oruj.schools.services.DailyReportService;
import azi.oruj.schools.services.SchoolService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DailyReportServiceImpl implements DailyReportService {
    private final DailyReportRepo dailyReportRepo;
    private final StudentsRepo studentsRepo;
    private final ModelMapper mapper;

    @Override
    public List<DailyReportDto> createFew(DailyReportCreateDto create) {
        if (!dailyReportRepo.existsByDailyDateAndSchoolNameAndWhatTime(create.getDailyDate(),
                create.getSchoolName(),create.getWhatTime())) {
            return mapper.map(dailyReportRepo.saveAll(dailyReports(create))
                    , new TypeToken<List<DailyReportDto>>() {
                    }.getType());
        }
        return null;
    }

    @Override
    public List<DailyReportDto> updateFew(Long id, DailyReportCreateDto update) {
        return null;
    }

    @Override
    public DailyReportDto getFew(Long id) {
        return null;
    }

    @Override
    public void deleteFew(Long id) {

    }
    @Override
    public List<String> getListWhatTime(String school) {
        return studentsRepo.findByWhatTime(school);
    }

    @Override
    public List<AllDailyListDto> getAllDailyList() {
        return dailyReportRepo.findAllDailyList();
    }

    @Override
    public List<DailyReportDto> getAll() {
        return null;
    }
    private List<DailyReport> dailyReports(DailyReportCreateDto create){
        LocalDate date= create.getDailyDate();
        LocalDateTime time= LocalDateTime.now();
        List<DailyReport> list = new ArrayList<>();
        List<DailyReportCreateBean> beanList= create.getBeanList();
        for (DailyReportCreateBean bean : beanList){
            DailyReport daily = new DailyReport();
            daily.setDailyDate(date);
            daily.setIsActive(true);
            daily.setIsAttended(bean.getIsAttended());
            daily.setSchoolName(create.getSchoolName());
            daily.setWhatTime(create.getWhatTime());
            daily.setStudent(mapper.map(bean.getStudent(), Students.class));
            daily.setCreateDate(time);
            list.add(daily);
        }
      return list;
    }
}
