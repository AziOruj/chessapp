package azi.oruj.schools.services.impl;

import azi.oruj.schools.entity.Students;
import azi.oruj.schools.entity.dto.StudentCreateDto;
import azi.oruj.schools.entity.dto.StudentDto;
import azi.oruj.schools.exceptions.SchoolNotFoundException;
import azi.oruj.schools.exceptions.StudentNotFoundException;
import azi.oruj.schools.repo.SchoolsRepo;
import azi.oruj.schools.repo.StudentsRepo;
import azi.oruj.schools.services.StudentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentsRepo studentsRepo;
    private final ModelMapper mapper;
    private final SchoolsRepo schoolsRepo;
    @Override
    public StudentDto create(StudentCreateDto create) {
        Students students = new Students();
        students.setSchool(schoolsRepo.findBySchoolName(create.getSchoolName()).orElseThrow(SchoolNotFoundException::new));
        students.setCreateDate(LocalDateTime.now());
        students.setBeginDate(create.getBeginDate());
        students.setName(create.getName());
        students.setSurname(create.getSurname());
        students.setWhatTime(create.getWhatTime());
        students.setIsActive(true);
        return mapper.map(studentsRepo.save(students),StudentDto.class);
    }

    @Override
    public StudentDto update(Long id, StudentCreateDto update) {
        Students students = studentsRepo.findById(id).orElseThrow(StudentNotFoundException::new);
        students.setSchool(schoolsRepo.findBySchoolName(update.getSchoolName()).orElseThrow(SchoolNotFoundException::new));
        students.setIsActive(true);
        students.setName(update.getName());
        students.setSurname(update.getSurname());
        students.setBeginDate(update.getBeginDate());
        students.setWhatTime(update.getWhatTime());
        students.setUpdateDate(LocalDateTime.now());
        return mapper.map(studentsRepo.save(students),StudentDto.class);
    }

    @Override
    public StudentDto get(Long id) {
        return mapper.map(studentsRepo.findById(id).orElseThrow(StudentNotFoundException::new),StudentDto.class);
    }

    @Override
    public List<StudentDto> getAll() {
        return mapper.map(studentsRepo.findAll(),new TypeToken<List<Students>> () {}.getType());
    }

    @Override
    public void delete(Long id) {
       if (studentsRepo.existsById(id)){
           studentsRepo.deleteById(id);
       }else {
          throw new SchoolNotFoundException();
       }
    }

    @Override
    public List<StudentDto> getAllSchool(String school, String whatTime, LocalDate beginDate) {
        return mapper.map(studentsRepo.findBySchool(school,whatTime,beginDate)
                ,new TypeToken<List<StudentDto>>() {}.getType());
    }


}
