package azi.oruj.schools.repo;

import azi.oruj.schools.entity.Students;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface StudentsRepo extends JpaRepository<Students, Long> {
    @Query(value = "select  u from Students u inner join fetch u.school r where r.schoolName=?1 and u.whatTime=?2 and u.isActive=true and u.beginDate<=?3")
    List<Students> findBySchool(String school, String whatTime, LocalDate beginDate);
    @Query(value = "select  u.whatTime from Students u  where u.school.schoolName=?1 and u.isActive=true group by u.whatTime")
    List<String> findByWhatTime(String school);
}
