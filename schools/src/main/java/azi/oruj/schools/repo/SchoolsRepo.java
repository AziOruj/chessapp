package azi.oruj.schools.repo;

import azi.oruj.schools.entity.SchoolEnt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface SchoolsRepo extends JpaRepository<SchoolEnt, Long> {
    Optional<SchoolEnt> findBySchoolName(String name);
    @Query(value = "select  u.schoolName from SchoolEnt u  where u.isActive=true")
    List<String> findAllString();
}
