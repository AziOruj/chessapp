package azi.oruj.schools.repo;

import azi.oruj.schools.entity.DailyReport;
import azi.oruj.schools.entity.dto.AllDailyListDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface DailyReportRepo extends JpaRepository<DailyReport, Long> {
//    @Query(value = " select daily.dailyDate from DailyReport daily inner join  daily.student stu inner join " +
//            " stu.school sch where daily.isActive=true and daily.dailyDate=?1 and daily.schoolName=?2 and daily.whatTime=?3")
    Boolean existsByDailyDateAndSchoolNameAndWhatTime(LocalDate dailyDate, String school, String whatTime);
    @Query(value = " select new azi.oruj.schools.entity.dto.AllDailyListDto(daily.dailyDate,daily.schoolName,daily.whatTime)" +
            "from DailyReport daily where daily.isActive=true group by daily.schoolName,daily.whatTime,daily.dailyDate")

    List<AllDailyListDto> findAllDailyList ();
}
